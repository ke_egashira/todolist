// ダイアログ表示
$(function (){
	$(".delete-submit").on("click", function() {
		if(window.confirm('Are you sure you want to delete this task？')){
			return true;
		}
		else{
			return false;
		}
	});
});

//タスクの表示切替
$(function() {
	$('.view-button1').click(function() {
		$(this).toggleClass("open");
	    $(this).next().slideToggle(200);
		});

	$('.view-button2').click(function() {
		$(this).toggleClass("open");
		$(this).next().slideToggle(200);
	});
});

//インプットボックスのフェードイン
$(function() {
  $('.info-message').hide().fadeIn('100');
  $('.input-text').hide().fadeIn('100');
});

$(function () {
   $('.tooltip').hide();
    $('.add-submit').hover(
    function () {
        $(this).next().fadeIn('fast');
    },
    function () {
    	$(this).next().fadeOut('fast');
    });
});

$(function() {
	$('span#limit-date-tooltip').hide();
	$('.edit-time').hover(function() {
		var that = this;
		sethover = setTimeout(function(){
		$(that).next().fadeIn('fast');},300)
	}, function() {
		$(this).next().fadeOut('fast')
		clearTimeout(sethover)
	});
});

$(function() {
    winW = $(window).width();
    spped = 300;
    $('p#errors').animate({
        width: winW
    }, spped);
});


