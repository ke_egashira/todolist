<!DOCTYPE html>

<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<html>
<head>
<meta charset="utf-8">
<title>EDIT TASK</title>
<link href="<c:url value="/resources/css/style.css" />" rel="stylesheet">
<link href="https://use.fontawesome.com/releases/v5.6.1/css/all.css" rel="stylesheet">
</head>
<body>

<div class="header">
			<h3>${message}</h3>
			<form:form modelAttribute="taskForm" action="${pageContext.request.contextPath}/task/insert/input/">
			<div class="insert-task">
			<input type="submit" class="fas fa-plus-circle  typesubmit" value="&#xf055; " >
				<table>
					<tbody>
						<tr>
							<td><form:input path="task"  class="input-text" placeholder="Add New Task"/></td>
						</tr>
					</tbody>
				</table>
				</div>
				<div><form:errors path="*" cssClass="errors"/></div>
			</form:form>
			</div>
		<br>
		<div class ="list-area">
<a href="${pageContext.request.contextPath}/task/" class="simple-label">HOME</a><br>
    <label class="editer-title"><i class="fas fa-info-circle"></i>Please edit this task</label>
    <form:form modelAttribute="taskForm" action="${pageContext.request.contextPath}/task/update/input/">
        <form:input type="hidden" path="id" value="${task.id}"/>
        <form:input type="hidden" path="isCleared" value="${task.isCleared}"/>
        <div style="main-content">
        <form:input path="task" class="edit-task" value="${updateTask.task}"/>
        <input type="submit" class="fas fa-arrow-alt-circle-right  check-submit" value="&#xf35a">
        </div>
        <div><form:errors path="*"  cssClass="errors"/></div>
    </form:form>
    </div>

</body>
</html>