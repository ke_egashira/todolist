<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">

<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<html>
<head>
<meta charset="utf-8">
<title>Home</title>
<link href="<c:url value="/resources/css/style.css" />" rel="stylesheet">
<link href="https://use.fontawesome.com/releases/v5.6.1/css/all.css" rel="stylesheet">
<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script type="text/javascript" src="<c:url value="/resources/js/script.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/countTimer.js" />"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.20.1/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/livestamp/1.1.0/livestamp.js"></script>

</head>
	<body onload="getCurrentTime()">
	<div id="main-wrapper">
		<div id="main">
			<div class="header">
				<div class="header-content">
					<div class="icon">
						<a href="${pageContext.request.contextPath}/task/"><img
							src="<c:url value="/resources/image/Title icon.png" />"
							width="auto" height="80px"></a>
					</div>
					<c:if test="${ not empty addData }">
						<form:form modelAttribute="taskForm"
							action="${pageContext.request.contextPath}/task/insert/input/">
							<div class="insert-task">
								<form:input path="task" class="input-text" placeholder="New Task" />
								<form:input path="limitDateDate" type="date" class="input-date" />
								<form:input path="limitDateTime" type="time" class="input-time" />
							</div>
							<input type="submit" class="fas fa-arrow-circle-right  add-submit"
								value="&#xf0a9; ">
							<p class="tooltip">Add a task..</p>
							<p id="errors"><form:errors path="*"/></p>
						</form:form>
					</c:if>

					<c:if test="${ empty addData }">
						<form:form modelAttribute="taskForm"
							action="${pageContext.request.contextPath}/task/update/input/">
							<form:input type="hidden" path="id" />
							<form:input type="hidden" path="isCleared"  />
							<div class="insert-task">
								<form:input path="task" class="input-text" />
								<form:input path="limitDateDate" type="date" class="input-date" />
								<form:input path="limitDateTime" type="time" class="input-time" />
							</div>
							<input type="submit" class="fas fa-arrow-circle-right  add-submit"
								value="&#xf0a9; ">
							<p class="tooltip">Edit a task..</p>
							<p id="errors"><form:errors path="*"/></p>
						</form:form>
					</c:if>
					<i class="far fa-clock"></i> <span id="input-nowdate"></span>
				</div>
			</div><br>

			<div class="list-area">
				<label class="statement">WAITING</label>
				<div class="view-button1">
					<input type="button" class="fas fa-chevron-up  view-button1" value="&#xf077; " />
				</div>
				<div class="view-wrapper1">
					<c:forEach items="${tasks}" var="task">
						<c:if test="${task.isCleared == 0 }">
							<div class="main-content1">
								<form:form modelAttribute="taskForm"
									action="${pageContext.request.contextPath}/task/update/input/">
									<form:hidden path="id" value="${task.id}" />
									<form:hidden path="task" value="${task.task}" />
									<form:hidden path="limitDate" value="${task.limitDate}" />
									<form:hidden path="isCleared" value="1" />
									<input type="submit" class="far fa-circle  type-submit" value="&#xf111; ">
								</form:form>
								<div class="waiting-list">
									<a href="${pageContext.request.contextPath}/task/update/input/${task.id}/"
										class="simple-label">${task.task}</a>
								</div>
								<form:form modelAttribute="taskForm"
									action="${pageContext.request.contextPath}/task/delete/input/">
									<form:input path="id" type="hidden" value="${task.id}" />
									<input type="submit" class="fas fa-trash-alt  delete-submit"
										value="&#xf2ed; ">
								</form:form>
							</div>
							<div class="viewtime-wrapper">
								<c:set var="today">${dateTime}</c:set>
								<fmt:parseDate var="date" value="${task.limitDate}" type="BOTH" pattern="yyyy-MM-dd HH:mm:ss.S"/>
								<c:set var="viewDate"><fmt:formatDate value="${date}" pattern="H:mm・M/d ,yyyy" /></c:set>
								<c:if test="${task.limitDate gt today }">
									<a href="${pageContext.request.contextPath}/task/update/input/${task.id}/"
										class="edit-time"><span data-livestamp="${task.limitDate}" id="count-time"></span></a>
									<span id="limit-date-tooltip">Due - ${viewDate}</span>
								</c:if>
								<c:if test="${task.limitDate lt today }">
									<a href="${pageContext.request.contextPath}/task/update/input/${task.id}/"
										class="edit-time"><span id="count-time-over">expired.</span></a>
									<span id="limit-date-tooltip">Due - ${viewDate}</span>
								</c:if>
							</div>

						</c:if>
					</c:forEach>
				</div>
				<br>
				<label class="statement">COMPLETED</label>
				<div class="view-button2">
					<input type="button" class="fas fa-chevron-up view-button2"
						value="&#xf077; " />
				</div>
				<div class="view-wrapper2">
					<c:forEach items="${tasks}" var="task" varStatus="status">
						<c:if test="${task.isCleared != 0 }">
							<div class="main-content2">
								<form:form modelAttribute="taskForm"
									action="${pageContext.request.contextPath}/task/update/input/">
									<form:hidden path="id" value="${task.id}" />
									<form:hidden path="task" value="${task.task}" />
									<form:hidden path="isCleared" value="0" />
									<form:hidden path="limitDate" value="${task.limitDate}" />

									<input type="submit" class="fas fa-check-circle  type-submit"
										value="&#xf058; ">
								</form:form>
								<div class="waiting-list">
									<a href="${pageContext.request.contextPath}/task/update/input/${task.id}/"
										class="simple-label">${task.task}</a>
								</div>

								<form:form modelAttribute="taskForm"
									action="${pageContext.request.contextPath}/task/delete/input/">
									<form:hidden path="id" value="${task.id}" />
									<input type="submit" class="fas fa-trash-alt  delete-submit"
										value="&#xf2ed; ">
								</form:form>
							</div>
							<div class="viewtime-wrapper">
								<c:set var="today">${dateTime}</c:set>
								<fmt:parseDate var="date" value="${task.limitDate}" type="BOTH" pattern="yyyy-MM-dd HH:mm:ss.S"/>
								<c:set var="viewDate"><fmt:formatDate value="${date}" pattern="H:mm・M/d ,yyyy" /></c:set>
								<c:if test="${task.limitDate gt today }">
									<a href="${pageContext.request.contextPath}/task/update/input/${task.id}/"
										class="edit-time"><span data-livestamp="${task.limitDate}" id="count-time"></span></a>
									<span id="limit-date-tooltip">Due - ${viewDate}</span>
								</c:if>
								<c:if test="${task.limitDate lt today }">
									<a href="${pageContext.request.contextPath}/task/update/input/${task.id}/"
										class="edit-time"><span id="count-time-over">expired.</span></a>
									<span id="limit-date-tooltip">Due - ${viewDate}</span>
								</c:if>
							</div>
						</c:if>
					</c:forEach>
				</div>
			</div>
			<footer>
				<p><small>Copyright © task manager. All rights reserved</small></p>
			</footer>
		</div>
	</div>
</body>
</html>
