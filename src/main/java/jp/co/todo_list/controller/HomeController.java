package jp.co.todo_list.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import jp.co.todo_list.dto.task.TaskDto;
import jp.co.todo_list.form.TaskForm;
import jp.co.todo_list.service.TaskService;

@Controller
public class HomeController {

	@Autowired
	private TaskService taskService;

	@RequestMapping(value = "/task/", method = RequestMethod.GET)
	public String taskAll(Model model) {

		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm");
		SimpleDateFormat dateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date date = new Date();

		List<TaskDto> tasks = taskService.getTaskAll();
		model.addAttribute("tasks", tasks);

		TaskForm form = new TaskForm();
		form.setLimitDateDate(dateFormat.format(date));
		form.setLimitDateTime(timeFormat.format(date));
		model.addAttribute("taskForm", form);

		String dateTimeText = (dateTimeFormat.format(date));
		model.addAttribute("dateTime", dateTimeText);
		model.addAttribute("addData", "addData");

		return "home";

	}

	@RequestMapping(value = "/task/insert/input/", method = RequestMethod.POST)
	public String taskInsert(@Valid @ModelAttribute TaskForm form, BindingResult result, Model model) {
		if (result.hasErrors()) {
			List<TaskDto> tasks = taskService.getTaskAll();
			model.addAttribute("tasks", tasks);
			form.setLimitDateDate(form.getLimitDateDate());
			form.setLimitDateTime(form.getLimitDateTime());
			model.addAttribute("taskForm", form);

			SimpleDateFormat dateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			Date date = new Date();
			String dateTimeText = (dateTimeFormat.format(date));
			model.addAttribute("dateTime", dateTimeText);
			model.addAttribute("addData", "addData");

			return "home";

		} else {
			taskService.insertTask(form);
		}
		return "redirect:/task/";
	}

	@RequestMapping(value = "/task/delete/input/", method = RequestMethod.POST)
	public String taskDelete(@ModelAttribute TaskForm form, Model model) {
		taskService.deleteTask(form);
		return "redirect:/task/";

	}

	@RequestMapping(value = "/task/update/input/{id}/", method = RequestMethod.GET)
	public String taskUpdate(Model model, @PathVariable int id) throws ParseException {
		TaskDto task = taskService.getTask(id);
		String infoMessage = "Please edit this task";
		model.addAttribute("updateTask", task);
		model.addAttribute("info", infoMessage);

		SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		SimpleDateFormat formatDate = new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat formatTime = new SimpleDateFormat("HH:mm");
		String limitDateTime = task.getLimitDate();

		try {
			Date dateNew = format1.parse(limitDateTime);
			String formatedDate = formatDate.format(dateNew);
			String formatedTime = formatTime.format(dateNew);

			TaskForm form = new TaskForm();
			form.setId(task.getId());
			form.setTask(task.getTask());
			form.setIsCleared(task.getIsCleared());
			form.setLimitDateDate(formatedDate);
			form.setLimitDateTime(formatedTime);
			model.addAttribute("taskForm", form);

			SimpleDateFormat dateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			Date date = new Date();
			String dateTimeText = (dateTimeFormat.format(date));
			model.addAttribute("dateTime", dateTimeText);

			List<TaskDto> tasks = taskService.getTaskAll();
			model.addAttribute("tasks", tasks);

			model.addAttribute("taskForm", form);

		} catch (ParseException e) {
			e.printStackTrace();
		}

		return "home";

	}

	@RequestMapping(value = "/task/update/input/", method = RequestMethod.POST)
	public String taskUpdate(@Valid @ModelAttribute TaskForm form, BindingResult result, Model model, int id) {
		if (result.hasErrors()) {
			List<TaskDto> tasks = taskService.getTaskAll();
			model.addAttribute("tasks", tasks);
			form.setLimitDateDate(form.getLimitDateDate());
			form.setLimitDateTime(form.getLimitDateTime());
			model.addAttribute("taskForm", form);

			SimpleDateFormat dateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			Date date = new Date();
			String dateTimeText = (dateTimeFormat.format(date));
			model.addAttribute("dateTime", dateTimeText);

			return "home";

		} else {
			taskService.updateTask(form);
			return "redirect:/task/";
		}
	}
}
