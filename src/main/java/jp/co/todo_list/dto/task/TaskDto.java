package jp.co.todo_list.dto.task;

import java.util.Date;

public class TaskDto {
	private Integer id;
    private String task;
    private String category;
	private Integer isCleared;
	private String limitDate;
	private Date createdDate;
	private Date updatedDate;

	private String limitDateDate;
	private String limitDateTime;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTask() {
		return task;
	}

	public void setTask(String task) {
		this.task = task;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public Integer getIsCleared() {
		return isCleared;
	}

	public void setIsCleared(Integer isCleared) {
		this.isCleared = isCleared;
	}

	public String getLimitDate() {
		return limitDate;
	}

	public void setLimitDate(String string) {
		this.limitDate = string;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public String getLimitDateDate() {
		return limitDateDate;
	}

	public void setLimitDateDate(String limitDateDate) {
		this.limitDateDate = limitDateDate;
	}

	public String getLimitDateTime() {
		return limitDateTime;
	}

	public void setLimitDateTime(String limitDateTime) {
		this.limitDateTime = limitDateTime;
	}
}
