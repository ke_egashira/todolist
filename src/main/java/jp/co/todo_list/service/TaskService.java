package jp.co.todo_list.service;

import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jp.co.todo_list.dto.task.TaskDto;
import jp.co.todo_list.entity.Task;
import jp.co.todo_list.form.TaskForm;
import jp.co.todo_list.mapper.TaskMapper;

@Service
public class TaskService {

	@Autowired
	private TaskMapper taskMapper;

	public TaskDto getTask(Integer id) {
		TaskDto dto = new TaskDto();
		Task entity = taskMapper.getTask(id);
		BeanUtils.copyProperties(entity, dto);
		return dto;
	}

	public List<TaskDto> getTaskAll() {
		List<Task> taskList = taskMapper.getTaskAll();
		List<TaskDto> resultList = convertToDto(taskList);
		return resultList;
	}

	private List<TaskDto> convertToDto(List<Task> taskList) {
		List<TaskDto> resultList = new LinkedList<>();
		for (Task entity : taskList) {
			TaskDto dto = new TaskDto();
			BeanUtils.copyProperties(entity, dto);
			resultList.add(dto);
		}
		return resultList;
	}

	@Transactional
	public void insertTask(TaskForm taskForm) {

		String limitDate = taskForm.getLimitDateDate() + " " + taskForm.getLimitDateTime();
		taskForm.setLimitDate(limitDate);

		TaskDto dto = new TaskDto();
		Task entity = new Task();
		BeanUtils.copyProperties(taskForm, dto);
		BeanUtils.copyProperties(dto, entity);
		taskMapper.insertTask(entity);
	}
	@Transactional
	public void deleteTask(TaskForm form) {
		int id = form.getId();
		taskMapper.deleteTask(id);
	}
	@Transactional
	public void updateTask(TaskForm form) {

		if (form.getLimitDate() == null) {
		String limitDate = form.getLimitDateDate() + " " + form.getLimitDateTime();
		form.setLimitDate(limitDate);
		}

		TaskDto dto = new TaskDto();
		Task entity = new Task();
		BeanUtils.copyProperties(form, dto);
		BeanUtils.copyProperties(dto, entity);
		taskMapper.updateTask(entity);
	}
}
