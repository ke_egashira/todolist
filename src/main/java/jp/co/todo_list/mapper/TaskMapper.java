package jp.co.todo_list.mapper;

import java.util.List;

import jp.co.todo_list.entity.Task;

public interface TaskMapper {
    Task getTask(int id);

    List<Task> getTaskAll();

    void insertTask(Task entity);

    void deleteTask(int id);

    void updateTask(Task entity);
}
