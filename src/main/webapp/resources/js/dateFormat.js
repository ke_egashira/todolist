function getCurrentTime() {
	const monthEnglishList = [ 'Jan.', 'Feb.', 'Mar.', 'Apr.', 'May', 'June',
			'July', 'Aug.', 'Sept.', 'Oct.', 'Nov.', 'Dec.' ]
	var now = new Date();

	var month = now.getMonth();
	var monthEnglish = monthEnglishList[month]
	var res = padZero(now.getHours()) + ":" + padZero(now.getMinutes()) + ":"
			+ padZero(now.getSeconds()) + " - " + monthEnglish + " "
			+ padZero(now.getDate()) + ", " + (now.getFullYear());
	document.getElementById('input-nowdate').innerHTML = res;

}
setInterval('getCurrentTime()', 1000);

// 先頭ゼロ付加
function padZero(num) {
	var result;
	if (num < 10) {
		result = "0" + num;
	} else {
		result = "" + num;
	}
	return result;
}