<!DOCTYPE html>

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
    <head>
        <meta charset="utf-8">
        <title>Management</title>
    </head>
    <body>
           <h1>${message}</h1>
        <c:forEach items="${users}" var="user">
            <p><c:out value="${user.name}"></c:out></p>
            <p><c:out value="${user.account}"></c:out></p>
            <p><c:out value="${user.branch}"></c:out></p>
            <p><c:out value="${user.department}"></c:out></p>
        </c:forEach>
    </body>
</html>